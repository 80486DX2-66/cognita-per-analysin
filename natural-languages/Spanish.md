# Spanish learning in English
## Progress
- [ ] [Basic Spanish grammar](#basic-spanish-grammar)
  - [x] [Nouns](#nouns)
     - [x] [Number](#number)
     - [x] [Gender](#gender)
     - [x] [Cases](#cases)
  - [x] [Verbs](#verbs)
  - [ ] [Adjectives](#adjectives)
    - [ ] [Agreement](#agreement) <!-- in number and gender with nouns -->
    - [ ] [Comparative form](#comparative-form)
    - [ ] [Superlative form](#superlative-form)
  - [ ] [Adverbs](#adverbs)
  - [ ] [Pronouns](#pronouns)
    - [ ] [Personal](#personal)
    - [ ] [Possessive](#possessive)
    - [ ] [Demonstrative](#demonstrative)
    - [ ] [Relative](#relative)
    - [ ] [Interrogative](#interrogative)
  - [ ] [Articles](#articles)
    - [ ] [Definite](#definite)
    - [ ] [Indefinite](#indefinite)
  - [ ] [Prepositions and their usage](#prepositions-and-their-usage)
  - [ ] [Conjunctions](#conjunctions)
    - [ ] [Coordinating](#coordinating)
    - [ ] [Subordinating](#subordinating)
- [x] [Sentence construction](#sentence-construction)
- [ ] [Spanish verb conjugation](#spanish-verb-conjugation)
  - [ ] [Moods](#moods)
    - [ ] [Indicative](#indicative)
    - [ ] [Subjunctive](#subjunctive)
    - [ ] [Imperative](#imperative)
  - [x] [Tenses](#tenses)
    - [x] [Present](#present)
    - [x] [Preterite](#preterite)
    - [x] [Imperfect](#imperfect)
    - [x] [Future](#future)
    - [x] [Conditional](#conditional)
    - [x] [Present perfect](#present-perfect)
    - [x] [Preterite perfect](#preterite-perfect)
    - [x] [Pluperfect](#pluperfect)
  - [ ] [Regular and irregular forms](#regular-and-irregular-forms)
- [x] [Common mistakes](#common-mistakes) <!-- made by English speakers learning Spanish -->
- [ ] [Common Spanish vocabulary](#common-spanish-vocabulary) <!-- numbers, colors, days of the week, months of the year, food, family, clothing, weather, etc. -->
- [x] [Useful Spanish phrases for everyday situations](#useful-spanish-phrases-for-everyday-situations) <!-- greetings, introductions, ordering food, asking for directions, making reservations, etc. -->
- [ ] [Pronunciation](#pronunciation)
  - [ ] [Vowels](#vowels)
  - [ ] [Consonants](#consonants)
  - [ ] [Diphthongs](#diphthongs)
- [ ] [Accent marks and their usage](#accent-marks-and-their-usage)
- [ ] [Differences between Spanish and English grammar and vocabulary](#differences-between-spanish-and-english-grammar-and-vocabulary)

## Basic Spanish grammar
Spanish grammar has many similarities to English grammar but also some key differences. Nouns, verbs, adjectives and other parts of speech have different rules for gender, number, conjugation and usage.

### Nouns

Nouns in Spanish have gender, which can be either masculine or feminine. They also have number, which can be singular or plural. The gender and number of nouns determines the form of articles, adjectives and verbs that accompany them. 

Nouns are divided into a few types:

1. Common nouns refer to general people, places or things. E.g. casa (house), amigo (friend)
2. Proper nouns refer to specific people, places or things. E.g. Juan, Madrid
3. Material nouns refer to materials. E.g. madera (wood), algodón (cotton)
4. Abstract nouns refer to ideas and qualities. E.g. felicidad (happiness), belleza (beauty)
5. Collective nouns refer to groups of people or objects. E.g. gente (people), equipo (team)

Most Spanish plural nouns are formed by adding -s or -es to the singular form. But there are also many exceptions and irregular plurals.

#### Number
Spanish nouns can be singular or plural. They take different forms for each number. Number is a grammatical feature that indicates quantity. The plural form is used for entities greater than one. Spanish marks number through morphological changes to the noun and agreement throughout the noun phrase.

Singular nouns refer to one entity. Masculine nouns often end in -o, feminine nouns in -a:
- libro = book
- mesa = table

Plural nouns refer to multiple entities. Most end in -s:
- libros = books
- mesas = tables

Some irregular plurals do not follow this pattern:
- mano = hand → manos = hands
- ojo = eye → ojos = eyes

Nouns ending in a consonant add -es:
- reloj = watch → relojes = watches

Number changes other elements like articles and adjectives:

Singular:
- el libro amarillo = the yellow book

Plural:
- los libros amarillos = the yellow books

#### Gender
In Spanish, all nouns have a grammatical gender of either masculine or feminine. Gender is a grammatical category that classes nouns based on the sex of their referents or according to a classification system of the language. Gender is marked morphologically on nouns and through agreement with modifiers.

| Gender | Suffix |
| :-- | :-- |
| Masculine | -án, -én, -ín, -ón, -ún, -ar, -er, -or, -miento, -aje |
| Feminine | -ción, -sión, -zón, -dad, -ed, -ncia, -tud, -ez, -eza |

For example:
- la mesa = the feminine table
- el libro = the masculine book

Gender is indicated by the form of determiners and adjectives that accompany the noun.

Masculine:
- el hombre alto = the tall man

Feminine:
- la mujer alta = the tall woman

#### Case
Spanish is a nominative-accusative language, marking these two primary cases. Case is a system used to indicate the grammatical role of a noun or pronoun. It marks case primarily on determiners and adjectives but not nouns themselves. Nominative indicates the subject, accusative the direct object.

| Case | Masculine | Feminine |
| :-- | :-- | :-- |
| Nominative | el | la |
| Accusative | lo | la |

- El niño come la manzana. = The boy eats the apple.
- Nominative: El libro es interesante. = The book is interesting.
- Accusative: Leí lo rápido. = I read it quickly.

Otherwise, case is not marked on Spanish nouns.

### Verbs
Spanish verbs indicate action and state of being. They conjugate to express tense, aspect, mood, number, person and gender and also have subjunctive and conditional forms, which are used to express doubt, possibility, or hypothetical situations. The verb conjugation changes to agree with the subject.

Verbs have the following conjugations in Spanish:
- Present: Yo como la manzana. = I eat the apple.
- Preterite: Yo comí la manzana. = I ate the apple.
- Imperfect: Yo comía la manzana. = I was eating the apple.
- Future: Yo comeré la manzana. = I will eat the apple.

Verbs also conjugate for three grammatical persons in both singular and plural forms:
- First person: Yo como/comemos = I eat / We eat
- Second person: Tú comes/coméis = You (singular) eat / You (plural) eat
- Third person: Él/Ella come/Ellos/Ellas comen = He/She eats / They eat

*Verb conjugations will be discussed in detail later.*

### Adjectives
<!-- W.I.P. -->

#### Agreement
<!-- W.I.P. -->

#### Comparative form
<!-- W.I.P. -->

#### Superlative form
<!-- W.I.P. -->

### Adverbs
<!-- W.I.P. -->

### Pronouns
<!-- W.I.P. -->

#### Personal
<!-- W.I.P. -->

#### Possessive
<!-- W.I.P. -->

#### Demonstrative
<!-- W.I.P. -->

#### Relative
<!-- W.I.P. -->

#### Interrogative
<!-- W.I.P. -->

### Articles
<!-- W.I.P. -->

#### Definite
<!-- W.I.P. -->

#### Indefinite
<!-- W.I.P. -->

### Prepositions and their usage
<!-- W.I.P. -->

### Conjunctions
<!-- W.I.P. -->

#### Coordinating
<!-- W.I.P. -->

#### Subordinating
<!-- W.I.P. -->

## Sentence construction
The basic ingredients for constructing Spanish sentences are:

- Nouns: Provide the subject and object of the sentence
- Verbs: Express the action or state of being
- Adjectives: Describe nouns
- Adverbs: Modify verbs, adjectives and other adverbs
- Pronouns: Replace nouns as subjects, objects, possessive adjectives
- Articles: Specify nouns as definite or indefinite
- Prepositions: Link nouns to other parts of the sentence
- Conjunctions: Join words and clauses within the sentence

The basic word order in Spanish is &lt;Subject&gt; &lt;Verb&gt; &lt;Object&gt; but word order can often be flexible for emphasis or style.

To construct a simple sentence:
1. Choose a subject noun or pronoun
2. Select an appropriate verb conjugated for the subject
3. Add an object noun or pronoun if needed
4. Add adjectives and adverbs to provide more detail
5. Add prepositional phrases to specify location, direction, etc.
6. Add conjunctions to join clauses

Examples:
- La ardilla come nueces rápidamente. = The squirrel eats nuts quickly.
- El gato duerme en la cama grande y cómoda. = The cat sleeps in the big and comfortable bed.

Questions are formed by inverting the places of subject and verb. Negatives are formed by adding "no" before the verb.

Examples:
- La ardilla come nueces. → ¿Come la ardilla nueces?
- La ardilla come nueces. → La ardilla no come nueces.

## Spanish verb conjugation
### Moods
<!-- W.I.P. -->

#### Indicative
<!-- W.I.P. -->

#### Subjunctive
<!-- W.I.P. -->

#### Imperative
<!-- W.I.P. -->

#### Present
The present tense is used to talk about actions that are currently happening, or habitual actions.

| Subject Pronoun | -ar Verb Ending | -er Verb Ending | -ir Verb Ending |
| :-- | :-- | :-- | :-- |
| yo = me | -o | -o | -o |
| tú = you, informal | -as | -es | -es |
| él / ella / usted = he / she / you, formal | -a | -e | -e |
| nosotros = we/us | -amos | -emos | -imos |
| vosotros = you &lbrack;all&rbrack;, informal | -áis | -éis | -ís |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | -an | -en | -en |

#### Preterite
The preterite tense is used to talk about completed actions in the past.

| Subject Pronoun | -ar Verb Ending | -er Verb Ending | -ir Verb Ending |
| :-- | :-- | :-- | :-- |
| yo = me | -é | -í | -í |
| tú = you, informal | -aste | -iste | -iste |
| él / ella / usted = he / she / you, formal | -ó | -ió | -ió |
| nosotros = we/us | -amos | -imos | -imos |
| vosotros = you &lbrack;all&rbrack;, informal | -asteis | -isteis | -isteis |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | -aron | -ieron | -ieron |

#### Imperfect
The imperfect tense is used to talk about actions that were ongoing or repeated in the past.

| Subject Pronoun | -ar Verb Ending | -er Verb Ending | -ir Verb Ending |
| :-- | :-- | :-- | :-- |
| yo = me | -aba | -ía | -ía |
| tú = you, informal | -abas | -ías | -ías |
| él / ella / usted = he / she / you, formal | -aba | -ía | -ía |
| nosotros = we/us | -ábamos | -íamos | -íamos |
| vosotros = you &lbrack;all&rbrack;, informal | -abais | -íais | -íais |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | -aban | -ían | -ían |

#### Future
The future tense is used to talk about actions that will happen in the future.

| Subject Pronoun | -ar Verb Ending | -er Verb Ending | -ir Verb Ending |
| :-- | :-- | :-- | :-- |
| yo = me | -é | -é | -é |
| tú = you, informal | -ás | -ás | -ás |
| él / ella / usted = he / she / you, formal | -á | -á | -á |
| nosotros = we/us | -emos | -emos | -emos |
| vosotros = you &lbrack;all&rbrack;, informal | -éis | -éis | -éis |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | -án | -án | -án |

#### Conditional
The conditional tense is used to talk about actions that would happen under certain conditions.

| Subject Pronoun | -ar Verb Ending | -er Verb Ending | -ir Verb Ending |
| :-- | :-- | :-- | :-- |
| yo = me | -ía | -ía | -ía |
| tú = you, informal | -ías | -ías | -ías |
| él / ella / usted = he / she / you, formal | -ía | -ía | -ía |
| nosotros = we/us | -íamos | -íamos | -íamos |
| vosotros = you &lbrack;all&rbrack;, informal | -íais | -íais | -íais |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | -ían | -ían | -ían |

#### Present Perfect
The present perfect tense is used to talk about completed actions in the past that have a connection to the present.

| Subject Pronoun | -ar Verb Ending | -er Verb Ending | -ir Verb Ending |
| :-- | :-- | :-- | :-- |
| yo = me | he -ado | he -ido | he -ido |
| tú = you, informal | has -ado | has -ido | has -ido |
| él / ella / usted = he / she / you, formal | ha -ado | ha -ido | ha -ido |
| nosotros = we/us | hemos -ado | hemos -ido | hemos -ido |
| vosotros = you &lbrack;all&rbrack;, informal | habéis -ado | habéis -ido | habéis -ido |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | han -ado | han -ido | han -ido |

#### Preterite Perfect
The preterite perfect tense is used to talk about completed actions in the past that happened before another past action.

| Subject Pronoun | Haber (Present Indicative) | Past Participle (-ado/-ido) |
| :-- | :-- | :-- |
| yo = me | he | -ado/-ido |
| tú = you, informal | has | -ado/-ido |
| él / ella / usted = he / she / you, formal | ha | -ado/-ido |
| nosotros = we/us | hemos | -ado/-ido |
| vosotros = you &lbrack;all&rbrack;, informal | habéis | -ado/-ido |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | han | -ado/-ido |

#### Pluperfect
The pluperfect tense is used to talk about completed actions in the past that happened before another past action.

| Subject Pronoun | Haber (Imperfect Indicative) | Past Participle (-ado/-ido) |
| :-- | :-- | :-- |
| yo = me | había | -ado/-ido |
| tú = you, informal | habías | -ado/-ido |
| él / ella / usted = he / she / you, formal | había | -ado/-ido |
| nosotros = we/us | habíamos | -ado/-ido |
| vosotros = you &lbrack;all&rbrack;, informal | habíais | -ado/-ido |
| ellos / ellas / ustedes = they / them / you &lbrack;all&rbrack;, formal | habían | -ado/-ido |

### Regular and irregular verb forms
<!-- W.I.P. -->

## Common mistakes
English speakers learning Spanish tend to make the following common mistakes:

1. Mistaking subject-verb agreement

Since English does not have strict subject-verb agreement rules, learners often forget to match verbs to the number and gender of the subject in Spanish.

2. Omitting obligatory pronouns

Since pronouns are often optional in English, learners may leave out obligatory subject and object pronouns in Spanish.

3. Confusing ¿cómo? and ¿cómo que?

Learners confuse the interrogative words "¿cómo?" meaning "how?" and "¿cómo que?" meaning "what do you mean? / what are you saying?".

4. Forgetting to place adjectives after nouns

Since adjectives typically precede nouns in English, learners place Spanish adjectives before nouns. But Spanish adjectives usually follow the noun.

5. Using the infinitive instead of the gerund

The -ing form in English can function as both the infinitive and gerund. Learners often incorrectly use the Spanish infinitive instead of the gerund where the gerund is required.

6. Confusing preterite and imperfect

The preterite and imperfect, used for different situations in the past, can be confusing for English speakers where a simple past tense is used for all past events.

As you progress in Spanish, be mindful of these common mistakes and review frequently to internalize the proper rules. Immersing yourself in Spanish through conversation, music, reading and writing will also help avoid these pitfalls over time.

## Common Spanish vocabulary
<!-- W.I.P -->

## Useful Spanish phrases for everyday situations
1. Greetings and farewells
- ¡Hola! = Hello!
- ¡Buenos días! = Good morning!
- ¡Buenas tardes! = Good afternoon!
- ¡Buenas noches! = Good evening! / Good night!
- Adiós = Goodbye
- Hasta luego = See you later
- Chao = Bye (informal)

2. Self-introduction
- Me llamo &lbrack;name&rbrack;. ¿Cómo te llamas tú? = My name is &lbrack;name&rbrack;. What's your name?
- Encantado (male speaker) / Encantada (female speaker). = Nice to meet you.
- ¿De dónde eres? = Where are you from?

3. Ordering food and drink
- Quisiera... = I would like...
- Un &lbrack;café/refresco&rbrack; por favor. = A &lbrack;coffee/soft drink&rbrack; please.
- La cuenta, por favor. = The bill, please.

4. Asking for directions
- ¿Cómo se va a...? = How do I get to...?
- ¿Está lejos? = Is it far?
- ¿A mano izquierda/derecha? = On the left/right?

5. Making reservations
- Quisiera reservar... = I would like to reserve/book...
- Una habitación doble = A double room
- Para &lbrack;fecha&rbrack; = For &lbrack;date&rbrack;

Memorizing common phrases for key situations will help you communicate effectively in everyday Spanish. Start with basics like greetings, self-introductions, ordering food and drink, asking directions and making reservations, then expand your repertoire as your vocabulary and confidence grow.

## Pronunciation
<!-- W.I.P. -->

### Vowels
<!-- W.I.P. -->

### Consonants
<!-- W.I.P. -->

### Diphthongs
<!-- W.I.P. -->

## Accent marks and their usage
<!-- W.I.P. -->

## Differences between Spanish and English grammar and vocabulary
<!-- W.I.P. -->
