#include <stdio.h>

int main(void) {
    long double x;

    x = .0123456789012345678901234567890123456789L;
        printf("Long double postfix: %.40Lf\n", x);
    x = .0123456789012345678901234567890123456789;
        printf("     Double postfix: %.40Lf\n", x);
    x = .0123456789012345678901234567890123456789f;
        printf("      Float postfix: %.40Lf\n", x);

    return 0;
}
