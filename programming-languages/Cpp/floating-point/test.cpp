#include <cfloat>
#include <iostream>

using namespace std;

#ifdef DBL_DECIMAL_DIG
# define OP_DBL_Digs (DBL_DECIMAL_DIG)
#else
# ifdef DECIMAL_DIG
#  define OP_DBL_Digs (DECIMAL_DIG)
# else
#  define OP_DBL_Digs (DBL_DIG + 3)
# endif
#endif

int main() {
    cout.precision(OP_DBL_Digs);

    cout <<
        "Test: 0.1 + 0.2" << endl <<
        "float\t\t= " << .1f + .2f << endl <<
        "double\t\t= " << .1 + .2 << endl <<
        "long double\t= " << .1L + .2L << endl;

    cout <<
        endl <<
        "Test: (1 + 2) / 10.0" << endl <<
        "float\t\t= " << (1 + 2) / 10.f << endl <<
        "double\t\t= " << (1 + 2) / 10. << endl <<
        "long double\t= " << (1 + 2) / 10.L << endl;

    cout <<
        endl <<
        "Test: 0.1 + 0.0001" << endl <<
        "float\t\t= " << .1f + .0001f << endl <<
        "double\t\t= " << .1 + .0001 << endl <<
        "long double\t= " << .1L + .0001L << endl;

    return 0;
}
