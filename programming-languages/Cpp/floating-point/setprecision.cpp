#include <iostream>
#include <cfloat>
#include <iomanip>

using namespace std;

int main() {
    cout <<
    	"Test: 0.1 + 0.2" << endl <<
    	"float\t\t= " << setprecision(1) << .1f + .2f << endl <<
    	"double\t\t= " << setprecision(1) << .1 + .2 << endl <<
    	"long double\t= " << setprecision(1) << .1L + .2L << endl;

    cout <<
    	endl <<
    	"Test: (1 + 2) / 10.0" << endl <<
    	"float\t\t= " << setprecision(1) << (1 + 2) / 10.f << endl <<
    	"double\t\t= " << setprecision(1) << (1 + 2) / 10. << endl <<
    	"long double\t= " << setprecision(1) << (1 + 2) / 10.L << endl;

    cout <<
    	endl <<
    	"Test: 0.1 + 0.0001" << endl <<
    	"float\t\t= " << setprecision(4) << .1f + .0001f << endl <<
    	"double\t\t= " << setprecision(4) << .1 + .0001 << endl <<
    	"long double\t= " << setprecision(4) << .1L + .0001L << endl;

    return 0;
}
