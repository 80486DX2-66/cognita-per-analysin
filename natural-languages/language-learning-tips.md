# Strategies for learning a foreign language

1. **Exposure through media**: Listen to music, watch TV shows and movies in
   the target language with subtitles if needed. This exposes you to natural
   conversational speech and helps vocabulary stick.

2. **Solve puzzles**: Games and puzzles that involve using the language can
   make learning fun while boosting vocabulary, grammar and fluency. Look for
   crosswords, puzzles, quizzes and match card games.

3. **Speaking practice**: Find a speaking partner or language exchange to
   practice conversations. Speaking out loud helps cement your grammar skills
   and exposes you to idioms/colloquial speech. Start simple and build up
   fluency over time.

4. **Read news or articles**: Following the news or reading magazine articles
   in the target language exposes you to a wide variety of topics and
   vocabulary. Start with easy readers if needed and gradually increase
   difficulty.

5. **Visit language learning sites/apps**: There are many great sites and apps
   that make language learning more interactive through games, audio exercises,
   flashcards and more. Duolingo, Memrise, Busuu and Drops are some good ones
   to explore.
