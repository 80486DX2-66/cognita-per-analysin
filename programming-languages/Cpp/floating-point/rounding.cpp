#include <cfloat>
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

/* borrowed from Stack Overflow */
template<class T>
static T Round(T a) {
    static_assert(std::is_floating_point<T>::value, "Round<T>: T must be floating point");

    return (a > 0) ? ::floor(a + static_cast<T>(0.5)) : ::ceil(a - static_cast<T>(0.5));
}

/* borrowed from Stack Overflow */
template<class T>
static T Round(T a, int places) {
    static_assert(std::is_floating_point<T>::value, "Round<T>: T must be floating point");

    const T shift = pow(static_cast<T>(10.0), places);

    return Round(a * shift) / shift;
}

int main() {
    float       a = Round(.1f + .2f, 1);
    double      b = Round(.1  + .2 , 1);
    long double c = Round(.1L + .2L, 1);

    cout << "Test: 0.1 + 0.2 == 0.3" << endl;
    if (a == .3f)
    	cout << "float 0.1 + 0.2 == 0.3" << endl;
    if (b == .3)
    	cout << "double 0.1 + 0.2 == 0.3" << endl;
    if (c == .3L)
    	cout << "long double 0.1 + 0.2 == 0.3" << endl;

    cout << endl << "Test: (1 + 2) / 10.0 == 0.3" << endl;
    a = Round((1 + 2) / 10.f, 1);
    b = Round((1 + 2) / 10. , 1);
    c = Round((1 + 2) / 10.L, 1);
    if (a == .3f)
    	cout << "float 0.1 + 0.2 == 0.3" << endl;
    if (b == .3)
    	cout << "double 0.1 + 0.2 == 0.3" << endl;
    if (c == .3L)
    	cout << "long double 0.1 + 0.2 == 0.3" << endl;

    cout << endl << "Test: 0.1 + 0.0001 == 0.1001" << endl;
    a = Round(.1f + .0001f, 4);
    b = Round(.1  + .0001 , 4);
    c = Round(.1L + .0001L, 4);
    if (a == .1001f)
    	cout << "float 0.1 + 0.0001 == 0.1001" << endl;
    if (b == .1001)
    	cout << "double 0.1 + 0.0001 == 0.1001" << endl;
    if (c == .1001L)
    	cout << "long double 0.1 + 0.0001 == 0.1001" << endl;

    return 0;
}
